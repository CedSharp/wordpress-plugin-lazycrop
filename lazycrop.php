<?php

/**
 * Plugin Name: Lazy Crop
 * Version: 1.0
 * Description: Lazily generate images on-the-fly with customizable focal points.
 * Author: Cedrik Dubois
 * Author Uri: https://cedsharp.ca
 */

use CedSharp\LazyCrop\Core;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
const LAZYCROP_PATH = __DIR__;
define( 'LAZYCROP_URL', plugin_dir_url( __FILE__ ) );

// Debug helper
if ( ! function_exists( 'dump' ) ) {
	function dump( $something, bool $with_colors = true ): void {
		if ( $with_colors ) {
			highlight_string(
				"<?php \$value=\n\n" . var_export( $something, true ) . "\n\n?>"
			);
		} else {
			echo '<pre>' . var_export( $something, true ) . '</pre>';
		}
	}

	function dump_and_die( $something, bool $with_colors = true ): void {
		dump( $something, $with_colors );
		die();
	}
}

// Disable all sizes and setup namespace class loader
require_once LAZYCROP_PATH . DIRECTORY_SEPARATOR . 'setup.php';

// Initialize LazyCrop
add_action( 'plugins_loaded', [ Core::get(), 'init' ] );

function lazycrop_add_size( string $name, int $width = 0, int $height = 0 ): void {
	Core::get()->add_size( $name, $width, $height );
}

function lazycrop_get_sizes(): array {
	return Core::get()->get_sizes();
}

function lazycrop_get_image( int $image_id, string $size = 'full', array $attr = [] ): string|null {
	return Core::get()->get_image( $image_id, $size, $attr );
}

function lazycrop_get_image_src( int $image_id, string $size = 'full' ): array|null {
	return Core::get()->get_image_src( $image_id, $size );
}

function lazycrop_get_image_if_exists( int $image_id, string $size = 'full', array $attr = [] ): string|null {
	return Core::get()->get_image_if_exists( $image_id, $size, $attr );
}

function lazycrop_get_image_src_if_exists( int $image_id, string $size = 'full' ): array|null {
	return Core::get()->get_image_src_if_exists( $image_id, $size );
}
