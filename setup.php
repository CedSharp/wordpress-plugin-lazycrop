<?php

namespace CedSharp\LazyCrop;

//
// Plugin hooks
//

// When uninstalling, delete all related folders
register_uninstall_hook(__FILE__, 'lazycrop_uninstall');
function lazycrop_uninstall(): void
{
    // TODO: Delete any temp folder
}

// Disable default wordpress sizes to avoid cropping
add_filter('intermediate_image_sizes', '__return_empty_array');
add_filter('intermediate_image_sizes_advanced', '__return_empty_array');

//
// Autoload namespace classes
//

function lazycrop_autoload(string $class_name): void
{
    if (strpos($class_name, 'CedSharp\\LazyCrop') === 0) {
        $sep = DIRECTORY_SEPARATOR;
        $file_name = basename(str_replace('\\', $sep, strtolower($class_name)));
        require_once LAZYCROP_PATH . "${sep}lib${sep}$file_name.php";
    }
}

spl_autoload_register(__NAMESPACE__ . '\\lazycrop_autoload');
