<template type="text/html" id="tmpl-focal-editor-size">
    <div class="focal-editor-size">
        <p class="focal-editor-size-name">{{ data.name }}</p>
        <div class="focal-editor-size-preview">
            <img src="{{ data.image }}" alt="preview {{ data.name }}">
        </div>
        <p class="focal-editor-size-dimensions">
            {{ data.width }}x{{ data.height }}
        </p>
    </div>
</template>

<template type="text/html" id="tmpl-focal-editor-size-uncropped">
    <div class="focal-editor-size uncropped">
        <p class="focal-editor-size-name">{{ data.name }}</p>
        <div class="focal-editor-size-preview">
            <img src="{{ data.image }}" alt="preview {{ data.name }}">
            <p>No focal point defined, click here to set one.</p>
        </div>
        <p class="focal-editor-size-dimensions">
            {{ data.width }}x{{ data.height }}
        </p>
    </div>
</template>

<template type="text/html" id="tmpl-focal-editor-size-fake">
    <div class="focal-editor-size fake">
        <p class="focal-editor-size-name">{{ data.name }}</p>
        <div class="focal-editor-size-preview"
             style="--crop-x: {{ data.crop.x }}; --crop-y: {{ data.crop.y }}; --crop-width: {{ data.crop.width }}; --crop-height: {{ data.crop.height }};">
            <img src="{{ data.image }}" alt="preview {{ data.name }}">
        </div>
        <p class="focal-editor-size-dimensions">
            {{ data.width }}x{{ data.height }}
        </p>
    </div>
</template>

<template type="text/html" id="tmpl-focal-editor-size-loading">
    <div class="focal-editor-size loading">
        <p class="focal-editor-size-name">{{ data.name }}</p>
        <div class="focal-editor-size-preview">
            <span class="spinner is-active"></span>
        </div>
        <p class="focal-editor-size-dimensions">
            {{ data.width }}x{{ data.height }}
        </p>
    </div>
</template>

<template type="text/html" id="tmpl-focal-editor-back">
    <button id="focal-editor-back">
    <span class="dashicons dashicons-arrow-left-alt2">
      <span class="screen-reader-text">Back</span>
    </span>
    </button>
</template>

<template type="text/html" id="tmpl-focal-editor-content">
    <p class="focal-editor-message">
        Updating focus point for <b>{{ data.size }}</b>.
    </p>
    <div class="focal-editor">
        <div class="focal-editor-cropper">
            <img src="{{ data.image }}" alt="preview focal point">
        </div>
    </div>
    <p>Move and resize the visible area of the image.</p>
    <footer class="focal-editor-footer">
        <button id="focal-cancel" class="button button-large">Cancel</button>
        <button id="focal-save" class="button button-primary button-large">
            <span class="spinner"></span>
            Save
        </button>
    </footer>
</template>