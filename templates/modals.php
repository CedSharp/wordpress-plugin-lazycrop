<template type="text/html" id="tmpl-lazy-modal">
    <div class="media-modal wp-core-ui lazy-modal" role="dialog" aria-labelledby="media-frame-title"
         style="margin: {{ data.margin }}px;">
        <div class="media-modal-content">
            <div class="media-frame-title">
                <div class="lazy-modal-title-start"></div>
                <h1 class="lazy-modal-title">{{ data.title }}</h1>
                <div class="lazy-modal-title-end">
                    <button class="media-modal-close">
                        <span class="media-modal-icon" style="background-image: none">
                            <span class="screen-reader-text">Close</span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="media-frame-content lazy-modal-content"></div>
        </div>
    </div>
    <div class="media-modal-backdrop lazy-backdrop"></div>
</template>