jQuery(() => {
    "use strict";

    /** @var {Backbone} Backbone */
    /** @type {jQuery} */
    const $ = jQuery;

    const LazyModalDef = {
        id:        "lazy_modal",
        events:    {
            "click .media-modal-close": "closeModal",
        },
        templates: {modal: undefined},

        getTitle() {
            return "Lazy Modal";
        },

        getMargin() {
            return 0;
        },

        initialize() {
            _.bindAll(this, "render", "closeModal");
            this.initialize_templates();
            this.render();
        },

        initialize_templates() {
            this.templates.modal = wp.template("lazy-modal");
        },

        render() {
            this.$el.append(
                this.templates.modal({
                                         title:  this.getTitle(),
                                         margin: this.getMargin(),
                                     })
            );
            $(document.body).append(this.$el);
        },

        append(el) {
            const $content = this.$(".lazy-modal-content");
            $content.append(el);
            return $content;
        },

        closeModal(e) {
            e.preventDefault();
            this.undelegateEvents();
            $(document).off("focusin");
            $(document.body).addClass({overflow: "auto"});
            this.remove();
        },
    };
    const LazyModal    = Backbone.View.extend(LazyModalDef);
    const lazycrop     = {
        modals: {},
        LazyModal,
    };

    // Install functionality in window for global access
    window.cedsharp          = window.cedsharp || {};
    window.cedsharp.lazycrop = lazycrop;
});
