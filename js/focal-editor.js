jQuery(() => {
    "use strict";

    /** @var {Object|undefined} focalEditorData */
    /** @type {jQuery} */
    const $ = jQuery;

    if (!window.cedsharp.lazycrop) throw new Error(
        "Expected LazyCrop module to exists!");

    if (!focalEditorData) throw new Error("Missing focal editor data!");

    const LazyModal = window.cedsharp.lazycrop.LazyModal;

    //#region FocalEditor

    const FocalEditorDef   = {
        id: 'focal-listing-modal',

        events: {
            ...LazyModal.prototype.events,
            "click #focal-editor-back": "closeModal",
        },

        getTitle() {
            return "Focal Points Editor";
        },

        getMargin() {
            return 8;
        },

        info: {
            image_id:    null, image_name: null, image_url: null,
            image_width: null, image_height: null,
            crops:       new Map(),
        },

        ui: {
            sizes: new Map(),
        },

        initialize() {
            const {
                      id, title, url, width, height
                  }                = wp.media.frames.edit.model.attributes;
            this.info.image_id     = id;
            this.info.image_name   = title;
            this.info.image_url    = url;
            this.info.image_width  = width;
            this.info.image_height = height;
            LazyModal.prototype.initialize.apply(this, arguments);
        },

        initialize_templates() {
            LazyModal.prototype.initialize_templates.apply(this, arguments);
            this.templates.size      = wp.template("focal-editor-size");
            this.templates.loading   = wp.template("focal-editor-size-loading");
            this.templates.back      = wp.template("focal-editor-back");
            this.templates.uncropped = wp.template(
                "focal-editor-size-uncropped");
        },

        render() {
            LazyModal.prototype.render.apply(this, arguments);
            this.$(".lazy-modal-title-start")
                .append(this.templates.back());
            this.$(".media-frame-content")
                .addClass("focal-editor-content");
            const sizes = Object.keys(focalEditorData.sizes);
            if (sizes.length) {
                for (const size of sizes) {
                    this.renderSize(size);
                    this.fetchSizePreview(size);
                }
            } else {
                this.renderNoSizes();
            }
        },

        makeBasicSizeInfo(sizeName) {
            const size       = focalEditorData.sizes[sizeName];
            const prettyName = sizeName
                .split("-")
                .map((n) => n[0].toUpperCase() + n.substring(1))
                .join(" ");
            return {size, prettyName};
        },

        makeSize(sizeName, image = null) {
            const img                = image || this.info.image_url;
            const {size, prettyName} = this.makeBasicSizeInfo(sizeName);
            return $(this.templates.size({
                name:   prettyName,
                width:  size.width,
                height: size.height,
                image:  img,
            }));
        },

        makeLoadingSize(sizeName) {
            const {size, prettyName} = this.makeBasicSizeInfo(sizeName);
            return $(this.templates.loading({
                name:   prettyName,
                width:  size.width,
                height: size.height,
            }));
        },

        makeNotCroppedSize(sizeName) {
            const {size, prettyName} = this.makeBasicSizeInfo(sizeName);
            return $(this.templates.uncropped({
                name:   prettyName,
                width:  size.width,
                height: size.height,
                image:  this.info.image_url,
            }));
        },

        renderSize(sizeName) {
            const $size = this.makeLoadingSize(sizeName);
            this.ui.sizes.set(sizeName, $size);
            $size.on('click', () => this.showEditor(sizeName));
            this.append($size);
        },

        renderNoSizes() {
            this.append($('<p>There are no sizes to crop images for.</p>'));
        },

        generateCroppedPreview(crop) {
            return new Promise((resolve => {
                const img  = new Image();
                img.src    = this.info.image_url;
                img.onload = () => {
                    const canvas  = document.createElement('canvas');
                    const ctx     = canvas.getContext('2d');
                    canvas.height = crop.height;
                    canvas.width  = crop.width;
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctx.drawImage(
                        img,
                        crop.x,
                        crop.y,
                        crop.width,
                        crop.height,
                        0,
                        0,
                        crop.width,
                        crop.height
                    );
                    resolve(canvas.toDataURL());
                };
            }));
        },

        fetchSizePreview(sizeName) {
            $.ajax({
                url:     focalEditorData.ajaxUrl,
                type:    'post',
                data:    {
                    action:   'get-focal-point',
                    nonce:    focalEditorData.nonces.getFocalPoint,
                    image_id: this.info.image_id,
                    size:     sizeName,
                },
                success: ({success, reason, image, crop}) => {
                    if (!success) {
                        console.error(reason);
                        return;
                    }

                    if (crop) this.info.crops.set(sizeName, crop);

                    /** @type {jQuery} */
                    const $size = this.ui.sizes.get(sizeName);
                    let $newSize;

                    if (crop && image) {
                        console.log(image);
                        // $newSize = this.makeSize(sizeName, image);
                    } else if (crop) {
                        this.generateCroppedPreview(crop)
                            .then((url) => {
                                $newSize = this.makeSize(sizeName, url);
                                $newSize.on(
                                    'click',
                                    () => this.showEditor(sizeName)
                                );
                                $size.replaceWith($newSize);
                                this.ui.sizes.set(sizeName, $newSize);
                            });
                    } else {
                        $newSize = this.makeNotCroppedSize(sizeName);
                    }

                    if ($newSize) {
                        $newSize.on('click', () => this.showEditor(sizeName));
                        $size.replaceWith($newSize);
                        this.ui.sizes.set(sizeName, $newSize);
                    }
                },
                error:   (ajax) => {
                    console.error(ajax.status, ajax.responseText);
                }
            });
        },

        closeModal(e) {
            LazyModal.prototype.closeModal.apply(this, arguments);
            FocalEditorModal.modal = null;
        },

        showEditor(sizeName) {
            const crop  = this.info.crops.get(sizeName);
            const size  = focalEditorData.sizes[sizeName];
            const image = this.info;
            FocalPointModal.open({crop, size, image, parent: this});
        },

        onSizeUpdated(size) {
            /** @type {jQuery} */
            const $size = this.ui.sizes.get(size.name);
            if ($size) {
                const $newSize = this.makeLoadingSize(size.name);
                $newSize.on('click', () => this.showEditor(size.name));
                $size.replaceWith($size);
                this.fetchSizePreview(size.name);
            }
        }
    };
    const FocalEditor      = LazyModal.extend(FocalEditorDef);
    const FocalEditorModal = {
        modal: null, open() {
            if (this.modal === null) this.modal = new FocalEditor();
        }, close() {
            if (this.modal !== null) this.modal.closeModal(new Event("click"));
        },
    };

    //#endregion

    //#region FocalPoint

    const FocalPointDef   = {
        id: 'focal-editor-modal',

        getTitle() {
            return "Set Focal Point";
        },

        getMargin() {
            return 16;
        },

        events: {
            ...LazyModal.prototype.events,
            "click #focal-cancel": "closeModal",
            "click #focal-save":   "saveFocalPoint",
        },

        info: {
            size:  null,
            image: null,
        },

        crop: {
            x:      0,
            y:      0,
            width:  0,
            height: 0,
        },

        initialize(options) {
            this.info.size      = options.size || null;
            this.info.image     = options.image || null;
            this.info.parent    = options.parent || null;
            this.info.crop      = options.crop || null;
            this.saveFocalPoint = this.saveFocalPoint.bind(this);
            LazyModal.prototype.initialize.apply(this, arguments);
        },

        initialize_templates() {
            LazyModal.prototype.initialize_templates.apply(this, arguments);
            this.templates.content = wp.template("focal-editor-content");
        },

        normalizeCrop(crop) {
            return {
                x:      typeof crop.x === 'number' ? crop.x : parseFloat(crop.x),
                y:      typeof crop.y === 'number' ? crop.y : parseFloat(crop.y),
                width:  typeof crop.width === 'number' ? crop.width : parseFloat(
                    crop.width),
                height: typeof crop.height === 'number' ? crop.height : parseFloat(
                    crop.height),
            };
        },

        render() {
            LazyModal.prototype.render.apply(this, arguments);

            this.append($(this.templates.content({
                image: this.info.image.image_url,
                size:  this.info.size.name
                           .split("-")
                           .map((n) => n[0].toUpperCase() + n.substring(
                               1))
                           .join(" "),
            })));

            // Calculate
            const ratio           = this.info.size.width / this.info.size.height;
            const imageRatio      = this.info.image.image_width / this.info.image.image_height;
            const $editor         = this.$(".focal-editor");
            const $container      = $editor.parent();
            const containerWidth  = $container.width();
            const containerHeight = $container.height() - 116;

            if ($container.height() >= containerWidth / imageRatio) {
                $editor.width(containerWidth);
                $editor.height(containerWidth / imageRatio);
            } else {
                $editor.width(containerHeight * imageRatio);
                $editor.height(containerHeight);
            }

            const data = {
                viewMode: 2, dragMode: "move", aspectRatio: ratio,
                autoCrop: true, rotatable: false, scalable: false,
                zoomable: false,
                preview:  this.$(".focal-editor-preview"),
                crop:     ({detail}) => (this.crop = {...detail}),
            };

            // Assign previously saved crop, otherwise make full-size
            if (this.info.crop) data.data = this.normalizeCrop(this.info.crop);
            else data.autoCropArea = 1;

            this.$(".focal-editor-cropper img")
                .cropper(data);
        },

        startLoading() {
            this.$(".focal-editor-footer button, .media-modal-close")
                .attr("disabled", "disabled")
                .find(".spinner")
                .addClass("is-active");
        },

        stopLoading() {
            this.$(".focal-editor-footer button, .media-modal-close")
                .removeAttr("disabled")
                .find(".spinner")
                .removeClass("is-active");
        },

        closeModal() {
            LazyModal.prototype.closeModal.apply(this, arguments);
            FocalPointModal.modal = null;
        },

        saveFocalPoint(e) {
            this.startLoading();

            $.ajax({
                type:    'post',
                url:     focalEditorData.ajaxUrl,
                data:    {
                    'action':   'set-focal-point',
                    'nonce':    focalEditorData.nonces.focalPoint,
                    'image_id': this.info.image.image_id,
                    'size':     this.info.size.name,
                    'crop':     {
                        x:      Math.round(this.crop.x),
                        y:      Math.round(this.crop.y),
                        width:  Math.round(this.crop.width),
                        height: Math.round(this.crop.height)
                    }
                },
                success: ({success, reason}) => {
                    if (success) {
                        this.updateParent();
                        this.closeModal(e);
                    } else {
                        this.stopLoading();
                        console.error(reason);
                    }
                },
                error:   ({status, responseText}) => {
                    this.stopLoading();
                    console.error(status, responseText);
                }
            });
        },

        updateParent() {
            if (this.info.parent)
                this.info.parent.onSizeUpdated(this.info.size);
        }
    };
    const FocalPoint      = LazyModal.extend(FocalPointDef);
    const FocalPointModal = {
        modal: null,

        open({crop, size, image, parent}) {
            if (this.modal === null) this.modal = new FocalPoint({
                size, image: image, parent, crop
            });
        },

        close() {
            if (this.modal !== null) this.modal.closeModal(new Event("click"));
        },
    };

    //#endregion

    window.cedsharp.lazycrop.FocalEditorModal = FocalEditorModal;
    window.cedsharp.lazycrop.FocalPointModal  = FocalPointModal;
});
