<?php

namespace CedSharp\LazyCrop;

class LazyModal {
	public static function init(): void {
		static::register_scripts();
	}

	public static function register_scripts(): void {
		wp_register_style( 'lazy-modal', LAZYCROP_URL . 'css/lazy-modal.css' );
		wp_register_script( 'lazy-modal', LAZYCROP_URL . 'js/lazy-modal.js', [
			'jquery',
			'backbone',
			'underscore',
			'wp-util',
		],                  false, true );
	}

	public static function enqueue_styles(): void {
		wp_enqueue_style( 'lazy-modal' );
	}

	public static function add_templates(): void {
		include_once LAZYCROP_PATH . '/templates/modals.php';
	}
}
