<?php

namespace CedSharp\LazyCrop;

class LazyFiles {
	public static function init(): void {
		add_filter( 'lazycrop_get_upload_dir',
		            [ static::class, 'get_upload_dir' ] );
	}

	public static function get_upload_dir( string $dir = '' ): string {
		if ( ! empty( $dir ) ) {
			return $dir;
		}

		$upload_dir = wp_get_upload_dir();

		return static::join( $upload_dir['basedir'], 'lazycrop' );
	}

	/** Helper which joins provided arguments with DIRECTORY_SEPARATOR */
	public static function join( string ...$pieces ): string {
		if ( empty( $pieces ) || ! is_array( $pieces ) ) {
			throw new \InvalidArgumentException( "No arguments provided!" );
		}
		$parts = [];
		foreach ( $pieces as $index => $piece ) {
			$parts =
				array_merge( $parts, static::split( $piece, $index == 0 ) );
		}
		$path = array_shift( $parts );
		foreach ( $parts as $part ) {
			if ( empty( $path ) || $path === DIRECTORY_SEPARATOR ) {
				$path .= $part;
			} else if ( $part != DIRECTORY_SEPARATOR ) {
				$path .= DIRECTORY_SEPARATOR . $part;
			}
		}

		return $path;
	}

	/** Splits a path into all it's components */
	public static function split( string $path, bool $is_first = false ): array {
		$starts_with_slash = false;
		if ( $is_first ) {
			$starts_with_slash = str_starts_with( $path, '/' )
			                     || str_starts_with( $path, '\\' );
		}
		if ( strpos( $path, '/', $starts_with_slash ? 1 : 0 ) ) {
			$parts = explode( '/', $path );
			if ( $starts_with_slash ) {
				$parts[0] = DIRECTORY_SEPARATOR;
			}
		} else if ( strpos(
			$path,
			DIRECTORY_SEPARATOR,
			$starts_with_slash ? 1 : 0
		) ) {
			$parts = explode( DIRECTORY_SEPARATOR, $path );
			if ( $starts_with_slash ) {
				$parts[0] = DIRECTORY_SEPARATOR;
			}
		} else {
			$parts = [ $path ];
		}

		return array_filter( $parts, fn( $val ) => ! empty( $val ) );
	}

	public static function get_upload_url( string $url = '' ): string {
		if ( ! empty( $url ) ) {
			return $url;
		}

		return get_home_url() . '/wp-content/uploads/lazycrop';
	}

	public static function get_image_path( array $image_meta ): string {
		$upload_dir = wp_get_upload_dir();

		return static::join( $upload_dir['basedir'], $image_meta['file'] );
	}

	public static function get_resized_image_path( string $image_path, object $size ): string {
		$upload_dir = apply_filters( 'lazycrop_get_upload_dir', '' );
		$info = (object) pathinfo( $image_path );
		$image_resized_path = $info->filename . "-resized-" . $size->width .
		                      'x' . $size->height . ".$info->extension";

		return static::join( $upload_dir, $image_resized_path );
	}

	public static function get_image_url( string $image_path ): string {
		$image_path = str_replace( ABSPATH, "/", $image_path );

		return str_replace( DIRECTORY_SEPARATOR, "/", $image_path );
	}

	public static function ensure_dir( string $dir ): bool {
		if ( is_dir( $dir ) ) {
			return true;
		}

		return wp_mkdir_p( $dir );
	}
}