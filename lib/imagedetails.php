<?php

namespace CedSharp\LazyCrop;

use JetBrains\PhpStorm\NoReturn;

class  ImageDetails {
	public static function init(): void {
		add_action( 'admin_enqueue_scripts',
		            [ static::class, 'enqueue_scripts' ] );
		add_action( 'admin_footer-upload.php',
		            [ static::class, 'add_templates' ] );

		add_filter( 'attachment_fields_to_edit', [
			static::class,
			'add_button_to_media_details'
		] );

		static::add_ajax(
			'set-focal-point',
			[ static::class, 'set_focal_point' ]
		);
		static::add_ajax(
			'get-focal-point',
			[ static::class, 'get_focal_point' ]
		);
		static::register_scripts();
	}

	protected static function add_ajax( string $action, array $handler ): void {
		add_action( "wp_ajax_nopriv_$action", $handler );
		add_action( "wp_ajax_$action", $handler );
	}

	public static function register_scripts(): void {
		// Focal Point
		wp_register_style(
			'focal-editor',
			LAZYCROP_URL . 'css/focal-editor.css'
		);
		wp_register_script(
			'focal-editor',
			LAZYCROP_URL . 'js/focal-editor.js',
			[
				'lazy-modal'
			],
			false,
			true
		);
	}

	#[NoReturn] public static function set_focal_point(): void {
		$image_id = $_REQUEST['image_id'] ?? null;
		$size     = $_REQUEST['size'] ?? null;
		$crop     = $_REQUEST['crop'] ?? null;
		$nonce    = $_REQUEST['nonce'] ?? null;

		if ( empty( $image_id ) || empty( $size ) || empty( $crop ) ||
		     empty( $nonce ) ) {
			wp_send_json( [
				              'success' => false,
				              'reason'  => 'Missing parameters'
			              ] );
		}

		if ( ! wp_verify_nonce( $nonce, 'set-focal-point' ) ) {
			wp_send_json( [
				              'success' => false,
				              'reason'  => 'Invalid nonce'
			              ] );
		}

		Core::get()->set_image_focal_point( $image_id, $size, $crop );
		wp_send_json( [ 'success' => true ] );
	}

	#[NoReturn] public static function get_focal_point(): void {
		$image_id = $_REQUEST['image_id'] ?? null;
		$size     = $_REQUEST['size'] ?? null;
		$nonce    = $_REQUEST['nonce'] ?? null;

		if ( empty( $image_id ) || empty( $size ) || empty( $nonce ) ) {
			wp_send_json( [
				              'success' => false,
				              'reason'  => 'Missing parameters'
			              ] );
		}

		if ( ! wp_verify_nonce( $nonce, 'get-focal-point' ) ) {
			wp_send_json( [
				              'success' => false,
				              'reason'  => 'Invalid nonce'
			              ] );
		}

		$focal_point   = Core::get()->get_image_focal_point( $image_id, $size );
		$resized_image =
			Core::get()->get_image_src_if_exists( $image_id, $size );

		wp_send_json( [
			              'success' => true,
			              'crop'    => $focal_point,
			              'image'   => $resized_image
		              ] );
	}

	public static function enqueue_scripts(): void {
		$screen = get_current_screen();
		if ( $screen->base == 'upload' || $screen->post_type == 'attachment' ) {
			// Cropper JS
			wp_enqueue_style(
				'cropper-js',
				'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/2.0.0-alpha.2/cropper.min.css',
				null,
				null
			);
			wp_enqueue_script(
				'cropper-js',
				'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/2.0.0-alpha.2/cropper.min.js',
				null,
				null,
				true
			);
			wp_enqueue_script(
				'cropper-jquery',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-cropper/1.0.1/jquery-cropper.min.js',
				[ 'jquery' ],
				null,
				true
			);

			// Modals
			LazyModal::enqueue_styles();

			// Focal Point
			wp_enqueue_style( 'focal-editor' );
			wp_localize_script( 'focal-editor', 'focalEditorData', [
				'sizes'   => Core::get()->get_sizes(),
				'ajaxUrl' => admin_url( 'admin-ajax.php' ),
				'nonces'  => [
					'focalPoint'    => wp_create_nonce( 'set-focal-point' ),
					'getFocalPoint' => wp_create_nonce( 'get-focal-point' ),
				],
			] );
			wp_enqueue_script( 'focal-editor' );
		}
	}

	public static function add_templates(): void {
		$screen = get_current_screen();
		if ( $screen->base == 'upload' || $screen->post_type == 'attachment' ) {
			LazyModal::add_templates();
			include_once LAZYCROP_PATH . '/templates/focal-editor.php';
		}
	}

	public static function add_button_to_media_details( array $fields ): array {
		ob_start();
		?>
        <button id="lazycrop" class="button button-primary">Edit Focal Points
        </button>
        <script type="text/javascript">
            jQuery(() => {
                const FocalEditor = window.cedsharp &&
                    window.cedsharp.lazycrop &&
                    window.cedsharp.lazycrop.FocalEditorModal;

                if (!FocalEditor) {
                    throw new Error('Missing focal editor modal!');
                }

                const $btn   = jQuery('#lazycrop');
                const $label = $btn.parent()
                                   .parent()
                                   .find('label');

                $btn.on('click', () => FocalEditor.open());
                $label.on('click', () => FocalEditor.open());
            });
        </script>
		<?php

		$html = ob_get_contents();
		ob_clean();

		$fields['lazycrop'] = [
			'label' => 'Lazy Crop',
			'input' => 'html',
			'html'  => $html,
		];

		return $fields;
	}
}
