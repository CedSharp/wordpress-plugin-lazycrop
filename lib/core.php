<?php

namespace CedSharp\LazyCrop;

use JetBrains\PhpStorm\NoReturn;

class Core {
	protected static Core $instance;
	protected array $sizes = [];

	public static function get(): Core {
		if ( ! isset( Core::$instance ) ) {
			Core::$instance = new Core();
		}

		return Core::$instance;
	}

	#[NoReturn] public function init(): void {
		LazyFiles::init();
		LazyModal::init();
		ImageDetails::init();
	}

	public function add_size( string $name, int $width = 0, int $height = 0 ): void {
		$this->sizes[ $name ] = [
			'name'   => $name,
			'width'  => $width,
			'height' => $height
		];
	}

	public function reset_image_focal_point( int $image_id, string $size ): void {
		$this->set_image_focal_point( $image_id, $size );
	}

	public function set_image_focal_point( int $image_id, string $size, array|null $focal_point = null ): void {
		if ( $focal_point === null ) {
			delete_post_meta( $image_id, "focal_point_$size" );
		} else {
			$data = [
				'x'      => $focal_point['x'] ?? 0,
				'y'      => $focal_point['y'] ?? 0,
				'width'  => $focal_point['width'] ?? 100,
				'height' => $focal_point['height'] ?? 100,
			];
			$data = serialize( $data );
			update_post_meta( $image_id, "focal_point_$size", $data );
		}

		$meta = wp_get_attachment_metadata( $image_id );
		if ( $meta && $this->has_size( $size ) ) {
			$image_path         = LazyFiles::get_image_path( $meta );
			$resized_image_path = LazyFiles::get_resized_image_path(
				$image_path,
				$this->get_size( $size )
			);
			if ( file_exists( $resized_image_path ) ) {
				wp_delete_file( $resized_image_path );
			}
		}
	}

	public function has_size( string $size ): bool {
		return array_key_exists( $size, $this->sizes );
	}

	public function get_size( string $size ): object {
		return (object) $this->sizes[ $size ];
	}

	public function get_image_if_exists( int $image_id, string $size = 'full', array $attr = [] ): string|null {
		if ( $image_id < 1 || empty( $size ) ) {
			return null;
		}

		if ( $size === 'full' ) {
			return wp_get_attachment_image( $image_id, $size, $attr );
		}

		$image = $this->get_image_src_if_exists( $image_id, $size );

		return $this->generate_image_html( $image, $attr );
	}

	public function get_image_src_if_exists( int $image_id, $size = 'full' ): array|null {
		if ( $image_id < 1 || ! empty( $size ) ) {
			return null;
		}
		if ( $size === 'full' ) {
			return wp_get_attachment_image_src( $image_id, $size );
		}

		$meta = wp_get_attachment_metadata( $image_id );
		if ( ! $meta || ! $this->has_size( $size ) ) {
			return null;
		}

		$size        = $this->get_size( $size );
		$source_path = LazyFiles::get_image_path( $meta );
		$target_path = LazyFiles::get_resized_image_path( $source_path, $size );

		if ( ! file_exists( $target_path ) ) {
			return null;
		}

		return [
			'src'    => $target_path,
			'width'  => $size->width,
			'height' => $size->height
		];
	}

	/**
	 * @param array|null $image
	 * @param array $attr
	 *
	 * @return string|null
	 */
	protected function generate_image_html( ?array $image, array $attr ): ?string {
		if ( empty( $image ) ) {
			return null;
		}

		$attr['width']   = $attr['width'] ?? $image['width'];
		$attr['height']  = $attr['height'] ?? $image['height'];
		$attr['loading'] = $attr['loading'] ?? 'lazy';

		$attrs = "";
		foreach ( $attr as $key => $value ) {
			if ( ! empty( $attrs ) ) {
				$attrs .= " ";
			}
			$attrs .= "$key=\"$value\"";
		}

		return "<img src=\"{$image['src']}\" alt=\"\" $attrs>";
	}

	public function get_image( int $image_id, string $size = 'full', array $attr = [] ): string|null {
		if ( $image_id < 1 || empty( $size ) ) {
			return null;
		}

		if ( $size === 'full' ) {
			return wp_get_attachment_image( $image_id, $size, $attr );
		}

		$image = $this->get_image_src( $image_id, $size );

		return $this->generate_image_html( $image, $attr );
	}

	public function get_image_src( int $image_id, string $size = 'full' ): array|null {
		if ( $image_id < 1 || empty( $size ) ) {
			return null;
		}

		if ( $size === 'full' ) {
			return wp_get_attachment_image_src( $image_id, $size );
		}

		$meta = wp_get_attachment_metadata( $image_id );
		if ( ! $meta || ! $this->has_size( $size ) ) {
			return null;
		}

		$size   = $this->get_size( $size );
		$source = (object) [
			'path'   => LazyFiles::get_image_path( $meta ),
			'height' => $meta['height'],
			'width'  => $meta['width']
		];
		$target = (object) [
			'path'   => LazyFiles::get_resized_image_path(
				$source->path,
				$size
			),
			'height' => $size->height,
			'width'  => $size->width
		];

		if ( file_Exists( $target->path ) ) {
			$image_size = getimagesize( $target->path );
			if ( empty( $image_size ) ) {
				return null;
			}

			return [
				'src'    => LazyFiles::get_image_url( $target->path ),
				'width'  => $target->width,
				'height' => $target->height
			];
		}

		if ( ! LazyFiles::ensure_dir( dirname( $target->path ) ) ) {
			return null;
		}

		$editor = wp_get_image_editor( $source->path );
		if ( is_wp_error( $editor ) ) {
			return null;
		}

		$focal_point = $this->get_image_focal_point( $image_id, $size->name );
		if ( $focal_point !== null ) {
			$editor->crop(
				$focal_point['x'],
				$focal_point['y'],
				$focal_point['width'],
				$focal_point['height']
			);
		}

		$editor->resize( $target->width, $target->height, true );
		$editor->save( $target->path );
		$size = $editor->get_size();

		return [
			'src'    => LazyFiles::get_image_url( $target->path ),
			'width'  => $size['width'],
			'height' => $size['height'],
		];
	}

	public function get_image_focal_point( int $image_id, string $size ): array|null {
		if ( empty( $size ) ) {
			return null;
		}
		$meta = get_post_meta( $image_id, "focal_point_$size", true );
		if ( empty( $meta ) ) {
			return null;
		}
		$data = maybe_unserialize( $meta );
		if ( ! is_array( $data ) ) {
			return null;
		}

		return $data;
	}

	public function get_sizes(): array {
		return $this->sizes;
	}
}
